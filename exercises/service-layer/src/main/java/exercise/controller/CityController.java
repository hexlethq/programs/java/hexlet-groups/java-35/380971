package exercise.controller;
import exercise.CityNotFoundException;
import exercise.model.City;
import exercise.model.Weather;
import exercise.repository.CityRepository;
import exercise.service.WeatherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;


@RestController
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private WeatherService weatherService;


    // BEGIN
    @GetMapping("/cities/{id}")
    public Weather getCitiesById(@PathVariable long id) {

        City city = cityRepository.findById(id)
                .orElseThrow(() -> new CityNotFoundException("City not found"));

        return weatherService.getWeatherForCity(city.getName());
    }

    @GetMapping("/search")
    public List<Map> getTemperatureByCityName(@RequestParam(required = false) String name) {
        List<City> cities = name == null ? cityRepository.findAllByOrderByName() : cityRepository.findCitiesByNameStartingWithIgnoreCase(name);

        return cities.stream()
                .map(city -> weatherService.getWeatherForCity(city.getName()))
                .map(weather -> Map.of("temperature", weather.getTemperature(),
                        "name", weather.getName()))
                .collect(Collectors.toList());
       // END
    }

}
