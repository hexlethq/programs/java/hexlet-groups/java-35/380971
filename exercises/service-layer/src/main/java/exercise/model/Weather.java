package exercise.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Weather {
    private String name;
    private int temperature;
    private int wind;
    private int humidity;
    private String cloudy;
}
