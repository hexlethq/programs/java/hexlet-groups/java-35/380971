package exercise;

// BEGIN
class Segment {

 //Point point = new Point();
   private Point getBeginPoint;
   private Point getEndPoint;

    public Segment(Point getBeginPoint, Point getEndPoint) {
        this.getBeginPoint = getBeginPoint;
        this.getEndPoint = getEndPoint;
    }

    public Point getBeginPoint() {
        return getBeginPoint;
    }

    public void setBeginPoint(Point getBeginPoint) {
        this.getBeginPoint = getBeginPoint;
    }

    public Point getEndPoint() {
        return getEndPoint;
    }

    public void setEndPoint(Point getEndPoint) {
        this.getEndPoint = getEndPoint;
    }
    public Point getMidPoint() {
        int midPointX = (getBeginPoint.getX() + getEndPoint.getX()) / 2;
        int midPointY = (getBeginPoint.getY() + getEndPoint.getY()) / 2;
        return new Point(midPointX, midPointY);
    }


}
// END
