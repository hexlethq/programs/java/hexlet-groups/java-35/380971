package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public class Connected implements Connection {

    private TcpConnection connection;

    public Connected(TcpConnection connection) {

        this.connection = connection;
    }

    @Override
    public String getCurrentState() {
        return "connected";
    }

    @Override
    public void connect() {
        System.out.println("Error! Already connected.");
    }

    @Override
    public void disconnect() {
        connection.setConnection(new Disconnected(this.connection));
    }

    @Override
    public void write(String data) {
        System.out.println("data");
    }

}
