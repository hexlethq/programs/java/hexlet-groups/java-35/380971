package exercise;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;

// BEGIN
public class App {
    public static List<Map<String, String>> findWhere(List<Map<String, String>> books, Map<String, String> dictionary) {
        List<Map<String, String>> foundBooks = new ArrayList<>();

        for (Map<String, String> book : books) {
            boolean isValuesMatching = true;
            for (String k : dictionary.keySet()) {
                if (!dictionary.get(k).equals(book.get(k))) {
                    isValuesMatching = false;
                }
            }
            if (isValuesMatching) {
                foundBooks.add(book);
            }
        }
        return foundBooks;
    }
}
//END
