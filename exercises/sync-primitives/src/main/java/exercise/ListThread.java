package exercise;

import java.util.concurrent.TimeUnit;

// BEGIN
public class ListThread extends Thread{

   private SafetyList list;

    public ListThread(SafetyList list) {
        this.list = list;
    }
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.list.add(i);
        }
    }

}

// END
