package exercise;

import java.util.Arrays;

class SafetyList {
    // BEGIN
    private int index = 0;
    private int lists[] = new int[index];

    public synchronized boolean add(int number){
        this.index += 1;
        this.lists = Arrays.copyOf(this.lists, this.index);
        this.lists[this.index - 1] = number;
        return true;
    }

    public int get(int i) {
        return this.index;
    }
    public int getSize() {
        return this.lists.length;
    }

    // END
}
