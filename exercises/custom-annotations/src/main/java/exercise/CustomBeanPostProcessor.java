package exercise;

import java.lang.reflect.Proxy;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// BEGIN
@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomBeanPostProcessor.class);

    private Map<Object, String> annotateBeans = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean.getClass().isAnnotationPresent(Inspect.class)) {
            String logLevel = bean.getClass()
                    .getAnnotation(Inspect.class)
                    .level();

            annotateBeans.put(bean, logLevel);
        }
        return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (annotateBeans.containsKey(bean)) {
            return (Proxy.newProxyInstance(
                    bean.getClass().getClassLoader(),
                    bean.getClass().getInterfaces(),
                    (proxy, method, args) -> {

                        String message = "Was called method: {}() with arguments: {}";

                        if (annotateBeans.get(bean).equals("info")) {
                            LOGGER.info(message, method.getName(), Arrays.toString(args));
                        } else {
                            LOGGER.debug(message, method.getName(), Arrays.toString(args));
                        }

                        return method.invoke(bean, args);
                    }
            ));
        }
        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }
}
// END
