package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static exercise.Data.getCompanies;


public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException {

        // BEGIN
        String search = request.getParameter("search");
        PrintWriter printWriter = response.getWriter();
        boolean flag = false;

        for (Object companys : getCompanies()) {
            String comp = companys instanceof String ? (String) companys : null;
            if (search != null) {
                if (comp.contains(search)) {
                    printWriter.write(comp + "\n");
                    flag = true;
                }
                } else {
                    printWriter.write(comp + "\n");
                flag = true;
            }

            }

            if (!flag) {
                printWriter.write("Companies not found");
            }
            printWriter.close();
            // END


        }
    }