package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;

import java.util.HashMap;
import java.util.Map;

import static exercise.App.getUsers;
import exercise.Users;

public class SessionServlet extends HttpServlet {

    private Users users = getUsers();

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        if (request.getRequestURI().equals("/login")) {
            showLoginPage(request, response);
            return;
        }

        response.sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doPost(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        switch (request.getRequestURI()) {
            case "/login":
                login(request, response);
                break;
            case "/logout":
                logout(request, response);
                break;
            default:
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    private void showLoginPage(HttpServletRequest request,
                               HttpServletResponse response)
                 throws IOException, ServletException {

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/login.jsp");
        requestDispatcher.forward(request, response);
    }

    private void login(HttpServletRequest request,
                               HttpServletResponse response)
                 throws IOException, ServletException {


        // BEGIN
        Map<String, String> userData = getUsers().findByEmail(request.getParameter("email"));
        if (userData != null && userData.get("password").equals(request.getParameter("password"))) {
            request.getSession().setAttribute("userId", userData.get("id"));
            request.getSession().setAttribute("flash", "Вы успешно вошли");

            response.sendRedirect("/");

        } else {
            request.getSession().setAttribute("flash", "Неверные логин или пароль");
            response.setStatus(422);
            request.setAttribute("user", userData);
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
        // END
    }

    private void logout(HttpServletRequest request,
                               HttpServletResponse response)
                 throws IOException, ServletException {

        // BEGIN
        request.getSession().setAttribute("flash", "Вы успешно вышли");
        request.getSession().removeAttribute("userId");

        response.sendRedirect("/");

        // END
    }
}
