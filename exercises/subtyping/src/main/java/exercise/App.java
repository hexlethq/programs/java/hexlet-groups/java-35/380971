package exercise;

import java.util.Map;

// BEGIN
class App {
    public static void swapKeyValue(KeyValueStorage dictionary) {
        Map<String, String> ditict = dictionary.toMap();
        for (String key : ditict.keySet()) {
           String value = dictionary.get(key,"");
            dictionary.set(value, key);
            dictionary.unset(key);
        }
    }
}
// END
