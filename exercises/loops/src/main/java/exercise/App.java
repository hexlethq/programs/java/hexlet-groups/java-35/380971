package exercise;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class App {
    // BEGIN
    public static void main(String[] args) throws Exception {
        String str = "Portable Network Graphics";

        System.out.println(App.getAbbreviation(str));
    }

    public static String getAbbreviation(String str) {
        String outPutString = "";
        boolean b = true;

        for (int index = 0; index < str.length(); index++) {
            if (str.charAt(index) == ' ') {
                b = true;
            } else if (str.charAt(index) != ' ' && b == true) {
                outPutString += (str.charAt(index));
                b = false;
            }
        }

            return outPutString.toUpperCase();

        }
    }
