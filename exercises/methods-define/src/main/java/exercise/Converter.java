package exercise;

class Converter {
    // BEGIN
    public static void main(String[] args) {
        int number = 10;
        String format = "b";
        convert(number, format);
        System.out.println(number + " " + "Kb" + " = " + Converter.convert(number, format) +  " " + format);
    }

    public static int convert(int number, String format) {

        if (format.equals("Kb")) {
            return (number / 1024);

        } else if (format.equals("b")) {
            return (number * 1024);
        } else
        return 0;

        }
    }

    // END
