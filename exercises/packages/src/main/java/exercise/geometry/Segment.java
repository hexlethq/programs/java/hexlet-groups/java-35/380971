// BEGIN
package exercise.geometry;

public class Segment {
    public static double[][] makeSegment(double[] pointStart, double[] pointEnd) {
        double[][] snippet = new double[2][2];
        snippet[0][0] = Point.getX(pointStart);
        snippet[0][1] = Point.getY(pointStart);
        snippet[1][0] = Point.getX(pointEnd);
        snippet[1][1] = Point.getY(pointEnd);
        return snippet;
    }

    public static double[] getBeginPoint(double[][] snippet) {
        double[] beginCoordinate = new double[2];
        //for x
        beginCoordinate[0] = snippet[0][0];
        //for y
        beginCoordinate[1] = snippet[0][1];
        return beginCoordinate;
    }

    public static double[] getEndPoint(double[][] snippet) {
        double[] endCoordinate = new double[2];
        endCoordinate[0] = snippet[1][0];
        endCoordinate[1] = snippet[1][1];
        return endCoordinate;
    }
}
// END
