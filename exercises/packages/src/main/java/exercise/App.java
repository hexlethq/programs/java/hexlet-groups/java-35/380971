// BEGIN
package exercise;
import exercise.geometry.Point;
import exercise.geometry.Segment;

public class App {

    public static double[] getMidpointOfSegment(double[][] snippet) {
        double[] middleCoordinate = new double[2];
        double[] beginCoordinate = Segment.getBeginPoint(snippet);
        double[] endCoordinate = Segment.getEndPoint(snippet);
        middleCoordinate[0] = (Point.getX(beginCoordinate) + Point.getX(endCoordinate)) / 2;
        middleCoordinate[1] = (Point.getY(beginCoordinate) + Point.getY(endCoordinate)) / 2;
        return middleCoordinate;
    }

    public static double[][] reverse(double[][] snippet) {
        double[] beginCoordinate = Segment.getBeginPoint(snippet);
        double[] endCoordinate = Segment.getEndPoint(snippet);
        double[][] reverseSegment = {
                {Point.getX(endCoordinate), Point.getY(endCoordinate)},
                {Point.getX(beginCoordinate), Point.getY(beginCoordinate)}};
        return reverseSegment;
    }

    public static boolean isBelongToOneQuadrant(double[][] segment) {
        boolean sameQuadrant;
        double[] beginCoordinate = Segment.getBeginPoint(segment);
        double[] endCoordinate = Segment.getEndPoint(segment);
        if (Point.getX(beginCoordinate) * Point.getX(endCoordinate) > 0 && Point.getY(beginCoordinate) * Point.getY(endCoordinate) > 0) {
            sameQuadrant = true;
        }else sameQuadrant = false;

        return sameQuadrant;
    }
}
// END
