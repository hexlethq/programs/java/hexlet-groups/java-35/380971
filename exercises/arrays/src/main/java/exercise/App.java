package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static void main(String[] args) {
        int[] numbers = {1, 2, -6, 3, 8};
        int[] numbers1 = {1, 4, 3, 4, 5};
        int[][] matrix2 = {{1, 2, 3}, {4, 5, 6}};
        reverse(numbers);
        mult(numbers1);
        flattenMatrix(matrix2);
    }

    public static int[] reverse(int[] numbers) {
        int[] reversed = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            reversed[i] = numbers[numbers.length - 1 - i];
        }
        return reversed;
    }

    public static int mult(int[] numbers1) {
        int result = 1;
        for (int i = 0; i < numbers1.length; i++)
            result *= numbers1[i];
        return result;
    }

    public static int[][] flattenMatrix(int[][] matrix2) {
        int[][] result = new int[matrix2.length][matrix2[0].length];
            for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 3; j++) {
                result[i][j] = matrix2[i][j];
            }
        }
        return result;
    }
}
    // END
