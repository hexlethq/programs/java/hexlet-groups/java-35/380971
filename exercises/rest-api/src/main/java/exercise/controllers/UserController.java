package exercise.controllers;

import io.javalin.http.Context;
import io.javalin.apibuilder.CrudHandler;
import io.ebean.DB;

import java.util.List;

import exercise.domain.User;
import exercise.domain.query.QUser;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.lang3.StringUtils;

public class UserController implements CrudHandler {

    public void getAll(Context ctx) {
        // BEGIN
        List<User> users = new QUser()
                .id.asc()
                .findList();
        ctx.json(DB.json().toJson(users));
        // END
    }


    public void getOne(Context ctx, String id) {

        // BEGIN
        User user = new QUser()
        .id.equalTo(Integer.parseInt(id))
        .findOne();
        ctx.json(DB.json().toJson(user));
        // END
    }


    public void create(Context ctx) {

        // BEGIN
        User user = ctx.bodyValidator(User.class)
                .check(it -> !it.getFirstName().isEmpty(), "Имя не должно быть пустым")
                .check(it -> !it.getLastName().isEmpty(), "Фамилия не должно быть пустым")
                .check(it -> EmailValidator.getInstance().isValid(it.getEmail()),"Неверный email")
                .check(it -> StringUtils.isNumeric(it.getPassword()), "Пароль должен состоять из цифр")
                .check(it -> it.getPassword().length() > 4, "Длина более 4х символов")
        .get();
        user.save();
        // END
    }


    public void update(Context ctx, String id) {
        // BEGIN
        User user = DB.json().toBean(User.class, ctx.body());
        user.setId(id);
        user.update();

        // END
    }


    public void delete(Context ctx, String id) {
        // BEGIN
        new QUser().id.equalTo(Integer.parseInt(id)).delete();
        // END
    }

}
