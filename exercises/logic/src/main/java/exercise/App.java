package exercise;

class App {
    public static void main(String[] args) {
        int number = 980;
        int minures = 16;
        App.sayEvenOrNot(number);
        App.printPartOfHour(minures);

    }
    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable = (number % 2 == 1 & number >= 1001);
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        if (number % 2 == 1) {
            System.out.println("no");
        } else
            System.out.println("yes");
    }
        // END

    public static void printPartOfHour(int minutes) {
        // BEGIN
        if (minutes <= 15) {
            System.out.println("First");
        } else if (minutes > 15 & minutes <= 30) {
            System.out.println("Second");
        } else if (minutes > 30 & minutes <= 45) {
            System.out.println("Third");
        } else if (minutes > 45 & minutes <= 60) {
            System.out.println("Fourth");
        }
        // END
    }
}
