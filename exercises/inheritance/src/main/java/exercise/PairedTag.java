package exercise;

import java.util.Map;
import java.util.List;


// BEGIN

public class PairedTag extends SingleTag {

    private String body;
    List<Tag> children;

    public PairedTag(String nameTag, Map<String, String> attributeTag, String body, List<Tag> children) {
        super(nameTag, attributeTag);
        this.body = body;
        this.children = children;
    }

    public String toString() {
        StringBuilder img = new StringBuilder(super.toString());

        for (Tag child : children) {
                img.append(child);
        }
        img.append(body).append("</").append(nameTag).append(">");

        return img.toString();
    }
}

// END
