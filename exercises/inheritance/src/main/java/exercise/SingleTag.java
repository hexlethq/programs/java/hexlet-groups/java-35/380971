package exercise;

import java.util.Map;

// BEGIN
public class SingleTag extends Tag{
       public SingleTag(String nameTag, Map<String, String> attributeTag) {
        super(nameTag, attributeTag);

    }
    public String toString() {
        StringBuilder imgs = new StringBuilder();
        imgs.append("<").append(nameTag).append(toTransform()).append(">");
        return imgs.toString();

    }
}
// END
