package exercise;

import java.util.Map;

// BEGIN
class Tag {
    public static void main(String[] args) {
    }
    protected String nameTag;
    protected Map<String, String> attributeTag;

    public Tag(String nameTag, Map<String, String> attributeTag) {
        this.nameTag = nameTag;
        this.attributeTag = attributeTag;
    }
    public String toTransform() {
        StringBuilder img = new StringBuilder();
        for (Map.Entry<String, String> entry : attributeTag.entrySet()) {
            img.append(" ").append(entry.getKey()).append("=\"").append(entry.getValue()).append("\"");
        }
        return img.toString();

    }

}
// END
