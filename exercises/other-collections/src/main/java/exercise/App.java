package exercise;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

// BEGIN
public class App {
    public static String getPossibleValues(Map<String, Object> data1, Map<String, Object> data2, String key) {
        if (data1.get(key) == null) {
            return "added";
        } else if (data2.get(key) == null) {
            return "deleted";
        } else if (data1.get(key).equals(data2.get(key))) {
            return "unchanged";
        } else {
            return "changed";
        }
    }
    public static Map<String, String> genDiff(Map<String, Object> data1, Map<String, Object> data2) {
        Set<String> keys = new TreeSet<>(data1.keySet());
        keys.addAll(data2.keySet());

        Map<String, String> compare = new LinkedHashMap<>();

        for (String key : keys) {
            compare.put(key, getPossibleValues(data1, data2, key));
        }

        return compare;
    }

}