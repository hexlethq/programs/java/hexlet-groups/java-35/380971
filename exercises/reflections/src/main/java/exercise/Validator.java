package exercise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

// BEGIN
class Validator {
    public static List<String> validate(Address address) {
        List<String> notValidFields = new ArrayList<>();
        for (Field fields : address.getClass().getDeclaredFields()) {
            NotNull notNull = fields.getAnnotation(NotNull.class);
            try {
                fields.setAccessible(true);
                if (notNull != null && fields.get(address) == null) {
                    notValidFields.add(fields.getName());
                }
            } catch (IllegalAccessException e) {
                e.getMessage();
            }
        }

        return notValidFields;
    }
}

// END
