package exercise;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static void main(String[] args) {

        String[][] image = {
                {"*", "*", "*", "*"},
                {"*", " ", " ", "*"},
                {"*", " ", " ", "*"},
                {"*", "*", "*", "*"},
        };
        for (String[] a : enlargeArrayImage(image)) {
            System.out.println(Arrays.toString(a));
        }
    }
    public static String[][] enlargeArrayImage(String[][] image) {
        String[][] getArray = Arrays.stream(image)
                .map(x -> Arrays.stream(x)
                        .flatMap(arr -> Stream.of(arr, arr))
                        .toArray(String[]::new))
                .flatMap(arr -> Stream.of(arr, arr))
                .toArray(String[][]::new);
        return getArray;
    }
}
// END
