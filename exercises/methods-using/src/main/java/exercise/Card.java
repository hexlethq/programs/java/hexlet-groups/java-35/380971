package exercise;

class Card {
    public static void main(String[] args) {
     String cardNumber = "2034399002125581";
     int starsCount = 4;
     Card.printHiddenCard(cardNumber, starsCount);
    }
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String stars = "*".repeat(starsCount);
        String hiddenCard = cardNumber.substring(12,16);
        System.out.println(stars + hiddenCard);
        // END
    }
}
