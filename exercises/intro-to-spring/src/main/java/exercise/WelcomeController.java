package exercise;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
// BEGIN
public class WelcomeController {

    @GetMapping("/")
    public String index() {
        return "Welcome to Spring";
    }
    @GetMapping("/hello")
        public String welcome(@RequestParam Optional<String> name) {
            return "Hello, " + name.orElseGet(() -> "World");
        }
}
// END
