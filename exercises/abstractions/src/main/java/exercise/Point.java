package exercise;

class Point {
    // BEGIN
    public static void main(String[] args) {

    }

    public static int[] makePoint(int xPoint, int yPoint) {
        return new int[]{xPoint, yPoint};
    }

    public static int getX(int[] inputPointX) {
        return inputPointX[0];
    }

    public static int getY(int[] inputPointY) {
        return inputPointY[1];
    }

    public static String pointToString(int[] PointToString) {
        return String.format("(%d, %d)", getX(PointToString), getY(PointToString));
    }

    public static int getQuadrant(int[] Point) {
        int x = getX(Point);
        int y = getY(Point);

        if (x > 0 && y > 0) {
            return 1;
        } else if (x < 0 && y > 0) {
            return 2;
        } else if (x < 0 && y < 0) {
            return 3;
        } else if (x > 0 && y < 0) {
            return 4;
        } else {
            return 0;
        }
    }
        // END

}

