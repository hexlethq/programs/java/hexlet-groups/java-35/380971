package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static void main(String[] args) {
        int[] numbers = {7, 1, 37, 5, 11, 8, 1};
        int[] numbers1 = {5, 4, -34, 8, 11, 5, 1};
        int[] arr = {11, -5, -45, 48, -4, -1};
        int[] arr2 = getElementsLessAverage(numbers);

        getIndexOfMaxNegative(arr);
        getElementsLessAverage(numbers);
        System.out.println(Arrays.toString(arr2));
        System.out.println(App.getSumBeforeMinAndMax(numbers1));
        System.out.println(App.getIndexOfMaxNegative(arr));
    }

    public static int getIndexOfMaxNegative(int[] arr) {
        int maxNegative = Integer.MIN_VALUE;
        int indexOfMaxNegative = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0 && maxNegative < arr[i]) {
                maxNegative = arr[i];
                indexOfMaxNegative = i;
            }
        }
        return indexOfMaxNegative;
    }

    public static int[] getElementsLessAverage(int[] numbers) {
        float averageOdds = averageOdds(numbers);
        int dstSize = countElements(numbers, averageOdds);
        int[] dstArray = new int[dstSize];
        for (int i = 0, j = 0; i < numbers.length; i++) {
            if (numbers[i] < averageOdds) {
                dstArray[j++] = numbers[i];
            }
        }
        return dstArray;
    }

    static int countElements(int[] array, float value) {
        int result = 0;
        for (int item : array) {
            if (item < value) {
                result++;
            }
        }
        return result;
    }

    static float averageOdds(int[] array) {

        float sum = 0;
        for (int i = 1; i < array.length; i += 1) {
            sum += array[i];
        }
        return sum / (array.length);
    }


    public static int getSumBeforeMinAndMax(int[] numbers1) {
        int min = numbers1[0];
        int max = min;
        int index_min = 0;
        int index_max = 0;
        for (int i = 0; i < numbers1.length; i++) {
            if (numbers1[i] < min) {
                min = numbers1[i];
                index_min = i;
            }
            if (numbers1[i] > max) {
                max = numbers1[i];
                index_max = i;
            }
        }
        int result = 0;
        for (int j = (index_min + 1); j != index_max; j++) {
            result = result + numbers1[j];
        }
        return result;
    }
}
 // END

