package exercise;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

// BEGIN
class App {
    public static long getCountOfFreeEmails(List<String> emails) {
        long frEmail = 0;

        frEmail = emails.stream()
                .filter(x -> freeEmail(x))
                .collect(Collectors.counting());
        return frEmail;
    }

    public static boolean freeEmail(String email) {
        return email.endsWith("@yandex.ru") || email.endsWith("@gmail.com") || email.endsWith("@hotmail.com");
    }
}
// END
