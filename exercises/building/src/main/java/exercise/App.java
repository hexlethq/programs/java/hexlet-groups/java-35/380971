// BEGIN
package exercise;

import com.google.gson.Gson;

class App {
    public static void main(String[] args) {
        System.out.print("Hello, World!");
    }

    public static String toJson(String[] str) {
        return new Gson().toJson(str);
    }
}

// END
