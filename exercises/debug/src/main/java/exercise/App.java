package exercise;

class App {
    // BEGIN
    public static void main(String[] args) throws Exception {
        int exam = 50;
        int project = 5;
        int length1 = 6;
        int length2 = 10;
        int length3 = 6;
        getTypeOfTriangle(length1, length2, length3);
        App.getFinalGrade(exam, project);
        System.out.println(App.getTypeOfTriangle(length1, length2, length3));
    }

    public static String getTypeOfTriangle(int length1, int length2, int length3) {

        if (length1 == length3 & length3 == length2 & length1 > 1) {
            return ("Равносторонний");
        } else if (length1 == length3 & length3 != length2 & length1 > 1 & length2 > 1) {
            return ("Равнобедренный");
        } else if (length1 != length3 & length1 > 1 & length2 > 1 & length3 > 1) {
            return ("Разносторонний");
        } else {
            return ("Треугольник не существует");
        }

    }

    public static void getFinalGrade(int exam, int project) {
                if (exam > 90 || project > 10) {
            System.out.println(100);
        } else if (exam > 75 & exam < 91 || project > 5 & project < 11) {
            System.out.println(90);
        } else if (exam > 50 & exam < 76 || project > 2 & project < 6) {
            System.out.println(75);
        } else
            System.out.println(0);
    }
}
    // END
