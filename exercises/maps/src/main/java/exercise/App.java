package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
class App {
    public static void main(String[] args) {
        String sentence = "java is the best programming language java";
        System.out.println(App.getWordCount(sentence));
    }
    public static Map<String, Integer> getWordCount(String sentence) {
        String[] words = sentence.split(" ");
        Map<String, Integer> wordCount = new HashMap<>();

        if (sentence.length() == 0) {
            return wordCount;
        }

        for (String word : words) {
            int variable = wordCount.getOrDefault(word, 0) + 1;
            wordCount.put(word, variable);
        }

        return wordCount;
    }

    public static int numOfWord(String word, String[] words) {
        int count = 0;

        for (String wordForCount : words) {
            if (word.equals(wordForCount)) {
                count++;
            }
        }

        return count;
    }
    public static String toString(Map<String, Integer> wordCount) {
        if(wordCount.isEmpty()) {
            return "{}";
        }
        String result = "{\n";
        for (Map.Entry<String, Integer> word : wordCount.entrySet()) {
            result += "  " + word.getKey() + ": " + word.getValue() + "\n";
        }

        return result + "}";
    }
}

//END
