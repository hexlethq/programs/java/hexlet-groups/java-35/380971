package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;

import java.nio.file.Paths;
import java.nio.file.Files;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUserbody(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        ObjectMapper objectMapper = new ObjectMapper();

        String file = Files.readString(Paths.get("src","main", "resources", "users.json"));

        return objectMapper.readValue(file, new TypeReference<List<Map<String, String>>>(){});
        // END
    }

    private void showUsers(HttpServletRequest request,
                           HttpServletResponse response)
            throws IOException {

        // BEGIN
        List<Map<String, String>> users = getUsers();
        StringBuilder body = new StringBuilder();
        users.forEach(user -> body.append("<html>" +
                        "<td>" + user.get("id") +"</td>" +
                        "<td>" + "<a href=/users/" + user.get("id") + ">" +
                        user.get("firstName") + " " + user.get("lastName") + "</a>" +
                        "</td>" +
                        "</tr>"));

        body.append("</html>");
        PrintWriter out = response.getWriter();
        out.println(body);
        // END
    }

    private void showUserbody(HttpServletRequest request,
                          HttpServletResponse response,
                          String id)
            throws IOException {

        // BEGIN
        List<Map<String, String>> users = getUsers();
        Map<String, String> user = users.stream()
                .filter(u -> u.get("id").equals(id))
                .findAny()
                .orElse(null);

        if (user == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        StringBuilder body = new StringBuilder();
        body.append("<html>" + user.get("id") +
                " " + user.get("firstName") +
                " " + user.get("lastName") +
                "</td></tr>" +
                "<tr><td>" + user.get("email") + "</td></tr>");

        body.append("""
                </table>
                    </body>
                        </html>
                """);
        PrintWriter out = response.getWriter();
        out.println(body);
        // END
    }
}
