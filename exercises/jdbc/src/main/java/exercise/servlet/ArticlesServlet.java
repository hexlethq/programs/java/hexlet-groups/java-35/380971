package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;

import java.sql.*;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import exercise.App;
import org.apache.commons.lang3.ArrayUtils;

import exercise.TemplateEngineUtil;


public class ArticlesServlet extends HttpServlet {

    private String getId(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return null;
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 1, null);
    }

    private String getAction(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return "list";
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 2, getId(request));
    }

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String action = getAction(request);

        switch (action) {
            case "list":
                showArticles(request, response);
                break;
            default:
                showArticle(request, response);
                break;
        }
    }

    private void showArticles(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException, ServletException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        List<Map<String, String>> articles = new ArrayList<>();
        int pageNumber = request.getParameter("page") == null ? 1 :
        Integer.parseInt(request.getParameter("page"));
        request.setAttribute("page", pageNumber);
        String artSql = "SELECT * FROM articles ORDER BY id LIMIT 10 OFFSET 10*(?-1)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(artSql);
            preparedStatement.setInt(1, pageNumber);

            ResultSet resalt = preparedStatement.executeQuery();

            while (resalt.next()) {
                Map<String, String> article = new HashMap<>();
                article.put("id", resalt.getString("id"));
                article.put("title", resalt.getString("title"));
                articles.add(article);

            }

            request.setAttribute("articles", articles);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // END
        TemplateEngineUtil.render("articles/index.html", request, response);
    }

    private void showArticle(HttpServletRequest request,
                             HttpServletResponse response)
            throws IOException, ServletException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        String id = getId(request);
        String artSql = "SELECT * FROM articles WHERE id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(artSql);
            preparedStatement.setInt(1, Integer.parseInt(id));

            ResultSet result = preparedStatement.executeQuery();

            Map<String, String> article = new HashMap<>();
            while (result.next()) {
                article.put("id", result.getString("id"));
                article.put("title", result.getString("title"));
                article.put("body", result.getString("body"));

            }
            if (article.size() == 0) {
                response.sendError(404);
            }
            request.setAttribute("article", article);

            result.close();
            preparedStatement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        // END
        TemplateEngineUtil.render("articles/show.html", request, response);
    }
}
