package exercise.controller;

import exercise.model.Comment;
import exercise.repository.CommentRepository;
import exercise.model.Post;
import exercise.repository.PostRepository;
import exercise.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;


@RestController
@RequestMapping("/posts")
public class CommentController {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;

    // BEGIN
    @GetMapping(path = "/{postId}/comments")
    public Iterable<Comment> getCommentsbyPostId(@PathVariable long postId) {
        return commentRepository.getCommentsByPostId(postId);
    }

    @GetMapping(path = "/{postId}/comments/{commentId}")
    public Comment getCommentByPostIdAndId(@PathVariable long postId,
                                           @PathVariable long commentId) {
        Comment comment = commentRepository.getCommentByPostIdAndId(postId, commentId);
        if (comment == null) {
            throw new ResourceNotFoundException("Comment not found");
        }
        return comment;
    }

    @PostMapping(path = "/{postId}/comments")
    public Comment createComment(@PathVariable long postId,
                                 @RequestBody Comment comment) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException("Not found"));

        comment.setPost(post);
        return commentRepository.save(comment);
    }

    @PatchMapping(path = "/{postId}/comments/{commentId}")
    public Comment updateComment(@PathVariable long postId,
                                 @PathVariable long commentId,
                                 @RequestBody Comment newComment) {

        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException("Not found"));

        Comment comment = commentRepository.getCommentByPostIdAndId(postId, commentId);
        if (comment == null) {
            throw new ResourceNotFoundException("Comment not found");
        }

        newComment.setId(commentId);
        newComment.setPost(post);
        return commentRepository.save(newComment);
    }

    @DeleteMapping(path = "/{postId}/comments/{commentId}")
    public void deleteComment(@PathVariable long postId,
                              @PathVariable long commentId) {

        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException("Not found"));

        Comment comment = commentRepository.getCommentByPostIdAndId(postId, commentId);
        if (comment == null) {
            throw new ResourceNotFoundException("Comment not found");
        }

        commentRepository.delete(comment);
    }
    // END
}
