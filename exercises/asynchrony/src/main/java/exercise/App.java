package exercise;

import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.CompletableFuture;

class App {

    // BEGIN
    private static String file1;

    private static String file2;

    private static String files;
    // END

    public static void main(String[] args) throws Exception {
        // BEGIN
        App.unionFiles(
                "/main/resources/file1.txt",
                "src/main/resources/file2.txt",
                "src/main/resources/destPath.txt"
        );
        // END
    }

    public static CompletableFuture<String> unionFiles(String path1, String path2, String generalFile) throws NoSuchFileException {

        CompletableFuture<String> readFile1 = CompletableFuture.supplyAsync(() -> {
            try {
                file1 = Files.readString(Paths.get(path1));
            } catch (IOException e) {
                System.out.println(e);
            }
            return file1;
        });

        CompletableFuture<String> readFile2 = CompletableFuture.supplyAsync(() -> {
            try {
                file2 = Files.readString(Paths.get(path2));
            } catch (IOException e) {
                System.out.println(e);
            }
            return file2;
        });

        CompletableFuture<String> resultFile = readFile1.thenCombine(readFile2, (first, second) -> {
            if (!Files.exists(Paths.get(generalFile))) {
                try {
                    Files.createFile(Paths.get(generalFile));
                } catch (IOException e) {
                    System.out.println(e);
                }
            }
            try {
                Files.writeString(Paths.get(generalFile), file1 + file2);
                files = Files.readString(Paths.get(generalFile));
            } catch (IOException e) {
                System.out.println(e);
            }
            return files;
        });

        return resultFile;
    }
}
