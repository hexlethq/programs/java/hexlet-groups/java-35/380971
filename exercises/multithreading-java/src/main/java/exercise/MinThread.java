package exercise;

// BEGIN
public class MinThread extends Thread {

    int[] number;
    int minNum;

    @Override
    public void run() {
        this.minNum = this.number[0];
        for (int num : this.number) {
            this.minNum = Math.min(this.minNum, num);
        }
    }
    public MinThread(int[] numbers) {
        this.number = numbers;
    }
    public int getMinNum() {
        return this.minNum;
    }
}
// END
