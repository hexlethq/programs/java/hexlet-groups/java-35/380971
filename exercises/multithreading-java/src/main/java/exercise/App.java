package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;




class App {
    private static final Logger LOGGER = Logger.getLogger("AppLogger");


    public static Map<String, Integer> getMinMax(int[] numbers) {
        Map<String, Integer> minMax = new HashMap<>();

        MaxThread maxThread = new MaxThread(numbers);
        MinThread minThread = new MinThread(numbers);

        LOGGER.info("Thread " + maxThread.getName() + " started");
        maxThread.start();

        LOGGER.info("Thread " + minThread.getName() + " started");
        minThread.start();

        try {
            maxThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            minThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        minMax.put("max", (maxThread.getMaxNum()));
        LOGGER.info(maxThread.getName() + " finished");

        minMax.put("min", (minThread.getMinNum()));
        LOGGER.info(minThread.getName() + " finished");
        return minMax;
            // END
    }
}
