package exercise;
import java.lang.Thread;
// BEGIN
public class MaxThread extends Thread {

    int[] number;
    int maxNum;

    @Override
       public void run() {
        this.maxNum = this.number[0];
           for (int num : this.number) {
               this.maxNum = Math.max(this.maxNum, num);
               }
           }
    public MaxThread(int[] numbers) {
        this.number = numbers;
    }
    public int getMaxNum() {
        return this.maxNum;
    }

}
// END