package exercise;

import javax.xml.crypto.Data;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

class App {
    // BEGIN
    public static void main(String[] args) {
        String[] arr = {"apples", "pears", "bananas"};
        String[][] arrOfUsersAndYear = {{"Aleksey Ivanov", "1995-02-15"}, {"Andrey Petrov", "1990-11-23"}};
        int year = 1990;
        buildList(arr);
        getUsersByYear(arrOfUsersAndYear, year);
        System.out.println(App.buildList(arr));
        System.out.println(App.getUsersByYear(arrOfUsersAndYear, year));

    }

    public static String buildList(String[] arr) {
        StringBuilder strHtml = new StringBuilder("<ul>\n");
        for (int i = 0; i < arr.length; i++) {
            strHtml.append("  ").append("<li>").append(arr[i]).append("</li>\n");
        }
        if (arr.length == 0) {
            return "";
        }
        return (strHtml.append("</ul>")).toString();
    }

    public static String getUsersByYear(String[][] arrOfPerson, int year) {
        StringBuilder nameAllPerson = new StringBuilder();
        for (int i = 0; i < arrOfPerson.length; i++) {

            LocalDate dateOfBorn = LocalDate.parse(arrOfPerson[i][1]);
            if (dateOfBorn.getYear() == year) {
                nameAllPerson.append("  <li>" + arrOfPerson[i][0] + "</li>\n");


            }
        }
        if (nameAllPerson.length() != 0) {
            nameAllPerson.insert(0, "<ul>\n");
            nameAllPerson.append("</ul>");
        }


        return nameAllPerson.toString();
    }

    // END
}