package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static void main(String[] args) {
        int[] number = {3, 10, 1, 3, 1, 9};
        sort(number);
        System.out.println(Arrays.toString(App.sort(number)));
    }
    public static int[] sort(int[] number) {
        int conunt;
        do {
            conunt = 0;
            for (int i = 0; i < number.length - 1; i++) {
                if (number[i] > number[i + 1]) {
                    int tmp = number[i];
                    number[i] = number[i + 1];
                    number[i + 1] = tmp;
                    conunt++;
                }
            }
        } while (conunt > 0);
        return number;
    }
    // END
}
