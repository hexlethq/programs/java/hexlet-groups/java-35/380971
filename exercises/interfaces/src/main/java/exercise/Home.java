package exercise;

import static java.lang.Double.compare;

// BEGIN
interface Home {
    double getArea();

    default int compareTo(Home another) {
        return compare(this.getArea(), another.getArea());
    }
}
// END
