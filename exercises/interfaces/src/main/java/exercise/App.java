package exercise;

import java.util.*;
import java.util.stream.Collectors;

// BEGIN
class App {
    public static List<String> buildAppartmentsList(List<Home> buildings, int numOrBuildings) {
        return buildings.stream()
                .sorted(Home::compareTo)
                .limit(numOrBuildings)
                .map(Home::toString)
                .collect(Collectors.toList());
        }
    }

// END
