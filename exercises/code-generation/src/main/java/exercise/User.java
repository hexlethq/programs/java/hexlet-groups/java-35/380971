package exercise;

import lombok.Value;

// BEG
@Value
// END
class User {
    int id;
    String firstName;
    String lastName;
    int age;
}
