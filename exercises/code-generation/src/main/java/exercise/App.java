package exercise;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

// BEGIN
class App {
    public static void save(Path pathToFile, Car car) {
      try {
          Files.writeString(pathToFile, car.serialize());
      } catch (IOException e) {
          e.printStackTrace();
      }
    }
    public static Car extract(Path car) {
        String json = null;
        try {
            json = Files.readString(car);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Car.unserialize(json);
    }
}
// END
