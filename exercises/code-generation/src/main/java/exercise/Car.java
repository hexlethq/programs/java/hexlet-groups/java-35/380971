package exercise;

import com.fasterxml.jackson.core.JsonProcessingException;

import lombok.Value;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

// BEGIN
@Value
// END
class Car {
    int id;
    String brand;
    String model;
    String color;
    User owner;
    static ObjectMapper objectMapper = new ObjectMapper();
    // BEGIN
    public String serialize() {
        String json = null;
try {
            json = objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return json;
    }

    public static Car unserialize(String JSON) {
        Car car = null;
        try {
            car = objectMapper.readValue(JSON, Car.class);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return car;
    }
    // END
}
