package exercise;


// BEGIN
class Kennel {
    public static void main(String[] args) {

    }

    public static String[][] puppiesNameAndBreed;

    public static void addPuppy(String[] puppy) {
        int puppiesCountBeforeAdding = puppiesNameAndBreed.length;
        String[][] tempPuppies = new String[puppiesCountBeforeAdding + 1][];
        for (int i = 0; i < puppiesNameAndBreed.length; i++) {
            tempPuppies[i] = puppiesNameAndBreed[i].clone();
        }
        //String[][] getPuppies;
        puppiesNameAndBreed = tempPuppies;
        puppiesNameAndBreed[puppiesCountBeforeAdding] = puppy;
    }


    public static void addSomePuppies(String[][] aLotOfPuppy) {
        for (String[] puppy : aLotOfPuppy) {
            Kennel.addPuppy(puppy);
        }
    }
    public static int getPuppyCount() {
        return puppiesNameAndBreed.length;
    }
    public static boolean isContainPuppy(String namePuppy) {
        for (String[] puppy : puppiesNameAndBreed) {
            if (puppy[0].equals(namePuppy)) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        return puppiesNameAndBreed.clone();
    }

    public static String[] getNamesByBreed(String breed) {
        int stepBreed = 0;
        for (String[] breedDog : puppiesNameAndBreed) {
            if (breedDog[1].equals(breed)) {
                stepBreed++;
            }
        }
        String[] result;
        int stepName = 0;
        result = new String[stepBreed++];
        for (String[] breedDog : puppiesNameAndBreed) {
            if (breedDog[1].equals(breed)) {
                result[stepName] = breedDog[0];
                stepName++;
            }
        }
        return result;
    }
    public static void resetKennel() {
        puppiesNameAndBreed = new String[0][];
    }
}

// END
