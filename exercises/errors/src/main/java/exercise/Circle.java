package exercise;

// BEGIN
class Circle {
   private Point pointCircle;
   private int radius;

    public Circle(Point pointCircle, int radius) {
        this.pointCircle = pointCircle;
        this.radius = radius;
    }
    int getRadius() {
        return this.radius;
    }
    double getSquare() throws NegativeRadiusException {
            if (getRadius() >= 0) {
                return Math.PI * getRadius() * getRadius();
        }else {
          throw new NegativeRadiusException();
        }

    }

}


// END
