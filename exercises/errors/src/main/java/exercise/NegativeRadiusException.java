package exercise;

// BEGIN
class NegativeRadiusException extends Exception {
   private String alarm;

    public NegativeRadiusException(String alarm) {
        this.alarm = alarm;
    }
    public NegativeRadiusException() {

    }
}
// END
