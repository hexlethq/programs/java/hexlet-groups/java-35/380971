package exercise;

import java.util.ArrayList;
import java.util.List;

// BEGIN
class App {
    public static void main(String[] args) {
        String letters  = "rkqodlw";
        String word = "woRld";
        System.out.println(App.scrabble(letters , word));
    }
    public static boolean scrabble(String letters, String word) {
        List<Character> characters = new ArrayList<>();
        for (Character chr : letters.toCharArray()) {
            characters.add(chr);
            System.out.println(chr);
        }

        word = word.toLowerCase();
        for (Character chr : word.toCharArray()) {
            if (!characters.remove(chr)) {
                return false;
            }
            System.out.println(chr);
        }

        return true;
    }
}
//END